﻿using System;

namespace euler6
{
    class Program
    {
        static void Main(string[] args)
        {
            int sumSquare, squareSum;
            sumSquare = squareSum = 0;

            for (int i = 1; i <= 100; i++)
            {
                squareSum += i;

                int squared;
                squared = Convert.ToInt32(Math.Pow(i, 2));
                sumSquare += squared;

            }
            squareSum = Convert.ToInt32(Math.Pow(squareSum, 2));

            Console.WriteLine("solution is: " + (squareSum - sumSquare));
            Console.Read();

        }
    }
}
