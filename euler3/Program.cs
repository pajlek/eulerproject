﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler3
{
    class Program
    {
        static void Main(string[] args)
        {
            long number, remainder;
            number = remainder = 600851475143;
            int maxFactorial = 0;
            for (int i = 2; !isPrime(remainder); i++)
            {
                if (remainder % i == 0)
                {
                    remainder = remainder / i;
                    if (isPrime(i) && (i > maxFactorial))
                    {
                        maxFactorial = i;
                        Console.WriteLine("prime factorial is " + maxFactorial);
                    }
                    i = 2;
                }
            }
            Console.WriteLine("Largest prime factorial of 600851475143 is: " + remainder);
            Console.ReadKey();
        }

        static public bool isPrime (long num)
        {
            if (num == 1) return false;

            for (int i = 2; i < Math.Sqrt(num); i++)
            {
                if (num % i == 0) return false;
            }
            return true;
        }

    }
}
