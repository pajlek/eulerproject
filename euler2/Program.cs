﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler2
{
    class Program
    {
        static void Main(string[] args)
        {
            int prevx = 1;
            int x = 2;
            int sum = 0;

            while (x < 4000000)
            { 
                if (x % 2 == 0) sum += x;
                x = prevx + x;
                prevx = x - prevx;
            }

            Console.WriteLine("Sum of even valued nums is: " + sum);
            Console.ReadKey();
        }
    }
}
