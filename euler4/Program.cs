﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler4
{
    class Program
    {
        static void Main(string[] args)
        {
            int product;
            int maxPalindrome = 0;

            for (int p1 = 100; p1 < 1000; p1++)
            {
                for (int p2 = 100; p2 < 1000; p2++)
                {
                    product = p1 * p2;
                    if (isPalindrom(p1 * p2) && product > maxPalindrome)
                    {
                        maxPalindrome = product;
                    }
                }
            }
            Console.WriteLine("The largest palindrome made from the product of two 3-digit numbers is " + maxPalindrome);
            Console.ReadLine();
        }

        static public bool isPalindrom (int num)
        {
            int[] intArray = new int[num.ToString().Length];

            int index = 0;
            while (num != 0)
            {
                intArray [index] = num % 10;
                num /= 10;
                index++;
            }

            int[] intArrayRev = new int[intArray.Length];
            Array.Copy(intArray, intArrayRev, intArray.Length);
            Array.Reverse(intArrayRev);

            for (int i = 0; i < intArray.Length; i++)
            {
                if (intArray[i] != intArrayRev[i]) return false;
            }
            return true;
        }
    }
}
