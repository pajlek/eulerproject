﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler7
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;

            int primeno10001 = 0;
            int primecount = 1;    
            for (int i = 1; primecount!=10001; i+=2)
            {
                if (isPrime(i)) primecount+=1;
                if (primecount == 10001) primeno10001 = i;
            }

            TimeSpan timeItTook = DateTime.Now - start;
            Console.WriteLine("10001th prime is: " + primeno10001 + "   // time took: " + timeItTook.TotalMilliseconds);
            Console.Read();
        }

        static public bool isPrime(long num)
        {
            if (num == 2) return true;
            else if (num == 1 || num % 2 == 0) return false;

            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0) return false;
            }
            return true;
        }
    }
}