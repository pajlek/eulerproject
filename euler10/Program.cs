﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler10
{
    class Program
    {
        static void Main(string[] args)
        {
            long sumOfPrimes = 2;

            for (int i = 1; i < 2000000; i+=2)
            {
                if (isPrime(i)) sumOfPrimes += i;
            }
            Console.WriteLine("sum of primes below 2000000 is: " + sumOfPrimes);
            Console.Read();
        }
        static public bool isPrime(long num)
        {
            if (num == 2) return true;
            else if (num == 1 || num % 2 == 0) return false;

            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0) return false;
            }
            return true;
        }

    }
}
