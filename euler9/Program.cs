﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler9
{
    class Program
    {
        static void Main(string[] args)
        {
            const int sum = 1000;
            
            for (int a = 1; a <= sum/3; a++)
            {
                for (int b = a + 1; b <= sum/2; b++) //b > a, so b+1
                {
                    int c = 1000 - b - a;   // a+b+c = 1000, so c = 1000-b-a

                    if (a*a + b*b == c*c)
                    {
                        Console.WriteLine("abc = " + a * b * c);
                        Console.Read();
                    }
                }
            }

        }
    }
}
