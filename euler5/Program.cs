﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler5
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0;
            do
            {
                number += 20;

            } while (!isDivisible(number));
            Console.WriteLine("solution: " + number);
            Console.Read();
        }

        static public bool isDivisible (int num)
        {
            for (int i = 20; i > 0; i--)
            {
                if (num % i != 0) return false;
            }
            return true;
        }
    }
}
