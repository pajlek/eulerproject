﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euler8
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;

            long maxProduct = 1;
            long localMaxProduct = 1;

            List<int> list = new List<int>();

            string body = File.ReadAllText("1000digits.txt");
            foreach (char c in body)  list.Add(c - '0'); //parse .txt , convert char to int for the list, add to list

            for (int i = 0; i < 988; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    localMaxProduct *= list[j+i];
                }

                if (localMaxProduct > maxProduct)
                {
                    maxProduct = localMaxProduct;
                }
                localMaxProduct = 1;
            }
            TimeSpan timeItTook = DateTime.Now - start;

            Console.WriteLine("maxProduct from 13 numers is: " + maxProduct + "  time it took: " + timeItTook.TotalMilliseconds);
            Console.Read();
        }
    }
}
